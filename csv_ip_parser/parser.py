"""
Url-parsing module for Comma Seperated Values files.
"""
import argparse
import collections.abc as types
import os
import re
from datetime import datetime
from multiprocessing import Pool
from urllib.parse import urlparse

import pandas as pd


def read_csv_file(csv_path: str, delimit=",") -> str:
    """
    Function to retrieve urls from .csv file

    :param csv_path: path to .csv file
    :param delimit: .csv delimiter
    :return: url list
    """
    dataset = pd.read_csv(csv_path, delimiter=delimit)
    return dataset.url

def parse_url(url: str) -> tuple:
    """
    Parsing url to format.

    :param url: url
    :return: parsed url tuple
    """
    if not url:
        return "", "", ""
    if not urlparse(url).scheme:
        url = "https://" + url
    parsed = urlparse(url)
    protocol = parsed.scheme
    host = parsed.netloc
    path = parsed.path

    if not host:
        host = parsed.scheme
        protocol = "https"
    return protocol, host, path


if __name__ == '__main__':
    CHUNK_SIZE = 50000
    PROCESSES = 300

    PARSER = argparse.ArgumentParser(
        description='Scan csv file and output for chunk-files')
    PARSER.add_argument('--url', metavar='URL', type=str,
                        help='link to .csv file', default="data.csv")
    CSV = PARSER.parse_args().url


    def worker(line: str) -> tuple:
        """Multiprocessor worker"""
        return parse_url(line)


    def chunks(workers: list, chunk_size: int) -> types.Iterable:
        """Yield successive n-sized chunks from lst."""
        for i in range(0, len(workers), chunk_size):
            yield workers[i:i + chunk_size]


    URLS_LIST = read_csv_file(CSV)

    POOL = Pool(processes=PROCESSES)
    WORKERS = POOL.map(worker, URLS_LIST)
    FILE_CHUNKS = chunks(WORKERS, CHUNK_SIZE)
    for chunk in FILE_CHUNKS:
        file_name = datetime.now().strftime("%m-%d-%Y-%H-%M-%S.%f.txt")[:-7]
        output_folder = "output"
        if not os.path.exists(output_folder):
            os.mkdir(output_folder)
        output_path = os.path.join(output_folder, file_name)
        with open(output_path, 'w+') as f:
            for item in chunk:
                f.write(f"Protocol: {item[0]} | Domain: {item[1]} | Path: {item[2]}\n")
