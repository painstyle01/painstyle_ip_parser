######## Parse basic .csv
python3 parser.py

######## Pass .csv from Internet
python3 parser.py --url [URL]

Outputs to folder in dir where script executed.
