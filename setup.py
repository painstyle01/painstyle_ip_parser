"""
Python module setup file
"""
from setuptools import setup, find_packages

with open('requirements.txt') as f:
    REQUIRED = f.read().splitlines()


setup(
    name='csv_ip_parser',
    version='1.1',
    packages=find_packages(),
    description="Simple python package to parse .csv file for hosts.",
    install_requires=REQUIRED,
)
