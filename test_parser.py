"""
Pytest testing cases
"""
from csv_ip_parser import parser as main


def test_parser():
    """
    Test most common variants of inputs using urlparser
    """
    assert main.parse_url("google.com") == ('https', 'google.com', '')
    assert main.parse_url("192.168.0.1/hello_world") == ('https', '192.168.0.1', '/hello_world')
    assert main.parse_url("ftp://192.168.0.1/root/Desktop/main.py") \
           == ('ftp', '192.168.0.1', '/root/Desktop/main.py')
    assert main.parse_url("") == ('', '', '')
    assert main.parse_url("ssh://hello") == ('ssh', 'hello', '')
